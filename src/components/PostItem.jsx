import React from 'react';
import MyButton from "./Ui/button/MyButton";

const PostItem = (props) => {
    return (
            <div className="NameOfClass">
            <div className="content">
                <strong>{props.count}. {props.post.title}</strong>
                <div>{props.post.body}</div>
            </div>
            <MyButton onClick ={()=>props.removePost(props.post)}>
                Удалить
            </MyButton>
        </div>
    );
};

export default PostItem;

