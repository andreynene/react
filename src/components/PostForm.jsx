import React, {useState} from 'react';
import MyInput from "./Ui/Input/MyInput";
import MyButton from "./Ui/button/MyButton";

const PostForm = () => {

    const [user, setUser] = useState({login: '', pass: ''})

    const addNewRecord = (e) => {
        e.preventDefault()

        setUser({login: '', pass: ''})
        alert('Пользователь '+ user.login + ' пароль '+ user.pass)

    }

    return (
        <form>
            <MyInput
                type="text"
                placeholder="Логин"
                value={user.login}
                onChange={e => setUser({...user, login: e.target.value})}
            />
            <MyInput
                type="text"
                placeholder="Пароль"
                value={user.pass}
                onChange={e => setUser({...user, pass: e.target.value})}
            />
            <MyButton onClick={addNewRecord}>Авторизоватся</MyButton>
        </form>
    );
};

export default PostForm;