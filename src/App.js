import React, {useMemo, useState} from "react";
import './App.css'
import PostList from "./components/PostList";
import PostForm from "./components/PostForm";
import MySelect from "./components/Ui/Select/MySelect";
import MyInput from "./components/Ui/Input/MyInput";

function App() {

  return (
      <div className="App">
        <PostForm />
        <hr style={{margin: '15px 0'}}/>

      </div>
  );
}

export default App;
